<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    /**
     * Create Property
     *
     * @return void
     */
    public function createProperties()
    {
        $this->post('/api/properties', ['country' => 'Australia','state'=>'NSW' ,'suburb'=>'Thornleigh'])
             ->seeJson([
                 'created' => true,
             ]);
    }

    /**
     * Get All Properties
     *
     * @return void
     */

    public function getAllProperties()
    {
        $this->get('/api/properties', [])
             ->seeJson([
                 'get' => true,
             ]);
    }
}
