<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class PropertyTest extends TestCase
{
    use DatabaseMigrations;
    // using it without middleware
    use WithoutMiddleware;
    
    protected static $property;
    

    public function setUp()
    {
        parent::setUp();
        $this->runDatabaseMigrations();
    }
    /**
     * test to check everything working 
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

   
    /**
     * Create Property
     *
     * @return void
     */
    public function createProperties()
    {
        $this->post('/api/properties', ['country' => 'Australia','state'=>'NSW' ,'suburb'=>'Thornleigh'])
             ->seeJson([
                 'created' => true,
             ]);
    }

    /**
     * Get All Properties
     *
     * @return void
     */

    public function getAllProperties()
    {
        $this->get('/api/properties', [])
             ->seeJson([
                 'get' => true,
             ]);
    }
}
