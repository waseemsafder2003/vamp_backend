<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CampaignController;
use \App\Http\Controllers\API\TeamController;
use \App\Http\Controllers\API\MembershipController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login');

// Route::group(['middleware' => 'auth:api'], function () {

Route::group(['prefix' => 'campaigns/', 'as' => 'campaigns.'], function () {

    Route::get('/', [CampaignController::class, 'all'])->name('all');
    Route::get('{id}', [CampaignController::class, 'detail'])->name('detail');
    Route::post('/add/', [CampaignController::class, 'store'])->name('store');
    Route::put('/update/{id}/', [CampaignController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [CampaignController::class, 'delete'])->name('delete');

});


Route::group(['prefix' => 'team/', 'as' => 'team.'], function () {

    Route::get('/', [TeamController::class, 'all'])->name('all');
    Route::get('{id}', [TeamController::class, 'detail'])->name('detail');
    Route::post('/add/', [TeamController::class, 'store'])->name('store');
    Route::put('/update/{id}/', [TeamController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [TeamController::class, 'delete'])->name('delete');

});

Route::group(['prefix' => 'membership/', 'as' => 'membership.'], function () {

    Route::get('/', [MembershipController::class, 'all'])->name('all');
    Route::get('{id}', [MembershipController::class, 'detail'])->name('detail');
    Route::post('/add/', [MembershipController::class, 'store'])->name('store');
    Route::put('/update/{id}/', [MembershipController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [MembershipController::class, 'delete'])->name('delete');

});


// });
