<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\MembershipCollection;
use App\Models\MembershipModel;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

use App\Models\MembershipModel as Membership;
use App\Http\Resources\MembershipCollection as MembershipResource;



class MembershipController extends BaseController
{

    public function __construct(MembershipModel $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $members = $this->model->getMembers();
        if(!is_null($members))
        {
            return $this->sendResponse(MembershipCollection::collection($members), 'Members retrieved successfully.');
        }
        else
        {
            return $this->sendError(' Error: Something went wrong.');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $membership = Membership::create($input);
        return $this->sendResponse(new MembershipResource($membership), 'Membership created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $membership = Membership::find($id);

        if (is_null($membership)) {
            return $this->sendError('Membership not found.');
        }

        return $this->sendResponse(new MembershipResource($membership), 'Membership retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Membership $membership)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => $this->name
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $membership->name = $input['name'];
        $membership->code = $input['code'];
        $membership->colorset = $input['colorset'];
        $membership->save();

        return $this->sendResponse(new MembershipResource($membership), 'Membership updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Membership $membership,$id)
    {

        $membership = Membership::find($id);
        if (is_null($membership)) {
            return $this->sendError('Membership not found.');
        }
        $membership->delete();
        return $this->sendResponse([], 'Membership deleted successfully.');
    }
}
