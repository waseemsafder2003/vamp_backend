<?php

namespace App\Http\Controllers\API;

use App\Models\CampaignModel;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

use App\Models\CampaignModel as Campaign;
use App\Http\Resources\CampaignCollection;

class CampaignController extends BaseController
{

    public function __construct(CampaignModel $model)
    {
        $this->model = $model;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $campaigns = $this->model->getCampaigns();
        if(!is_null($campaigns))
        {
            return $this->sendResponse(CampaignCollection::collection($campaigns), 'Campaigns retrieved successfully.');
        }
        else
        {
            return $this->sendError(' Error: Something went wrong.');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

   //id	name	start_date	end_date	budget	hashtags	team_id	description
        $validator = Validator::make($input, [
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'budget' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $campaign = Campaign::create($input);
        return $this->sendResponse(new CampaignCollection($campaign), 'Campaign created successfully.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $campaign = $this->model->getSingleCampaign($id);

        if (is_null($campaign)) {
            return $this->sendError('Campaign not found.');
        }
   
        return $this->sendResponse(new CampaignCollection($campaign[0]), 'Campaign retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $campaignId)
    {
        $input = $request->all();

        $campaign = Campaign::find($campaignId);
        if(!is_null($campaign))
        {
            foreach ($input as $key=>$value)
            {
                $campaign->$key = $value;
            }

        }
        $campaign->save();
        $campaign->campaignId= $campaignId;
        return $this->sendResponse(new CampaignCollection($campaign), 'Campaign updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Campaign $campaign,$id)
    {
        $campaign = Campaign::find($id);
        if (is_null($campaign)) {
            return $this->sendError('Campaign not found.');
        }
        $campaign->delete();
        return $this->sendResponse([], 'Campaign deleted successfully.');
    }
}
