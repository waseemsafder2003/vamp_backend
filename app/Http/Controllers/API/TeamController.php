<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\TeamCollection;
use App\Models\TeamModel;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

use App\Models\TeamModel as Team;
use App\Http\Resources\TeamCollection as TeamResource;



class TeamController extends BaseController
{

    public function __construct(TeamModel $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $teams = $this->model->getTeams();
        if(!is_null($teams))
        {
            return $this->sendResponse(TeamCollection::collection($teams), 'Teams retrieved successfully.');
        }
        else
        {
            return $this->sendError(' Error: Something went wrong.');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $team = Team::create($input);
        return $this->sendResponse(new TeamResource($team), 'Team created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $team = Team::find($id);

        if (is_null($team)) {
            return $this->sendError('Team not found.');
        }

        return $this->sendResponse(new TeamResource($team), 'Team retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $teamId)
    {
        $input = $request->all();
        $team = Team::find($teamId);
        if(!is_null($team))
        {
            foreach ($input as $key=>$value)
            {
                $team->$key = $value;
            }
        }
        $team->save();
        return $this->sendResponse(new TeamCollection($team), 'Team updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Team $team,$id)
    {

        $team = Team::find($id);
        if (is_null($team)) {
            return $this->sendError('Team not found.');
        }
        $team->delete();
        return $this->sendResponse([], 'Team deleted successfully.');
    }
}
