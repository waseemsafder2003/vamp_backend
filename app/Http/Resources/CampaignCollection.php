<?php

namespace App\Http\Resources;
   
use Illuminate\Http\Resources\Json\JsonResource;
   
class CampaignCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'team_id' => $this->team_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'budget' => $this->budget,
            'tags' => $this->tags,
            'campaign_name' => $this->campaignName,
            'description' => $this->description,
            'campaign_id' => $this->campaignId,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}