<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class TeamCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'color_set' => $this->colorset,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
