<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class CampaignModel extends Model
{

    protected $table = 'campaigns';


     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'budget',
        'tags',
        'description',
    ];


    public function getCampaigns() {
        $campaigns = DB::select("SELECT *,  `camp`.name as campaignName,  `camp`.id as campaignId
                                    FROM `campaigns` AS camp
                                    JOIN `teams` AS tm ON camp.`team_id`=tm.`id`
                                    ");

        return $campaigns;
    }

    public function getSingleCampaign($id) {
        $campaigns = DB::select("SELECT  *,  `camp`.name as campaignName,  `camp`.id as campaignId
                                    FROM `campaigns` AS camp
                                    JOIN `teams` AS tm ON camp.`team_id`=tm.`id`
                                    WHERE `camp`.id = $id
                                    ");

        return $campaigns;
    }

}
