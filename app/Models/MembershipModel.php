<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MembershipModel extends Model
{

    protected $table = 'memberships';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'team_id',

    ];
    public function getMembers() {
        $members = DB::select("SELECT *, `teams`.name as teamname 
                                      FROM `memberships` 
                                      JOIN `teams` on teams.id = `memberships`.team_id
                                      JOIN  `users` on users.id =  `memberships`.user_id
                                      order by `memberships`.created_at DESC");
        return $members;
    }
    
}
