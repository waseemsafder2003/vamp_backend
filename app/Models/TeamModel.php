<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TeamModel extends Model
{

    protected $table = 'teams';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'code',
        'colorset',
    ];

    public function getTeams() {
        $teams = DB::select("SELECT * FROM `teams` order by created_at DESC");
        return $teams;
    }
    
}
