<?php

use Illuminate\Database\Seeder;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class CampaignsTableSeeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // specify relative to Laravel project base path
        // specify filename that is automatically dumped from an external process
        $this->file = '/database/seeds/campaigns.csv';
        
        $this->worksheetTableMapping = [
            'Campaigns' => 'campaigns'
        ];
        
        $this->aliases = [
            'id' => 'id',
            'name' => 'name',
            'start_date' => 'start_date',
            'end_date' => 'end_date',
            'budget' => 'budget',
            'hashtags' => 'tags',
            'team_id' => 'team_id',
            'description' => 'description',
        ];
        
        parent::run();
    }
}
