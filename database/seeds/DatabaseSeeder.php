<?php

use Illuminate\Database\Seeder;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserTableSeeder::class,
            CampaignsTableSeeder::class,
            MembershipsTableSeeder::class,
            //TagsTableSeeder::class,
            TeamTableSeeder::class,
            //CampaignsTagsSeeder::class,
        ]);   
    }
}
