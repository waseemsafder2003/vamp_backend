<?php

use Illuminate\Database\Seeder;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class UserTableSeeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // specify relative to Laravel project base path
        // specify filename that is automatically dumped from an external process
        $this->file = '/database/seeds/users.csv';

        $this->worksheetTableMapping = [
            'Users' => 'users'
        ];

        $this->aliases = [
            'id' => 'id',
            'full_name' => 'name',
            'email' => 'email',
            'mobile_phone' => 'mobile_phone',
            'password' => 'password',
        ];

        parent::run();
    }
}