<?php

use Illuminate\Database\Seeder;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class TeamTableSeeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // specify relative to Laravel project base path
        // specify filename that is automatically dumped from an external process
        $this->file = '/database/seeds/teams.csv';

        $this->worksheetTableMapping = [
            'Teams' => 'teams'

        ];

        $this->aliases = [
            'id' => 'id',
            'name' => 'name',
            'code' => 'code',
            'color_set' => 'colorset',

        ];

        parent::run();
    }
}
