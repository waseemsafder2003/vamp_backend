<?php

use Illuminate\Database\Seeder;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class MembershipsTableSeeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // specify relative to Laravel project base path
        // specify filename that is automatically dumped from an external process
        $this->file = '/database/seeds/memberships.csv';

        $this->worksheetTableMapping = [
            'Memberships' => 'memberships'
        ];

        $this->aliases = [

            'id' => 'id',
            'team_id' => 'team_id',
            'user_id' => 'user_id',
        ];

        parent::run();
    }
}
