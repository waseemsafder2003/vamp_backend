users
=====

| id |        email         |   full_name    | mobile_phone |                           password                           |
|----|----------------------|----------------|--------------|--------------------------------------------------------------|
| 1  | bob@gomail.com       | Bob Brown      | 444123123    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 2  | anna@gomail.com      | Anna Apple     | 444987987    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 3  | sam@gomail.com       | Sam Spade      | 444456456    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 4  | kate@hatmail.com     | Kate Key       | 422123123    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 5  | tom@hatmail.com      | Tom Thompson   | 422987987    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 6  | min@macrosoft.com    | Min Ming       | 422456456    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 7  | vijesh@macrosoft.com | Vijesh Vine    | 423789789    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 8  | carlos@macrosoft.com | Carlos Crimson | 423123123    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 9  | prue@macrosoft.com   | Prue Prado     | 447456654    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
| 10 | don@ozmail.com       | Don Drapper    | 445321123    | $2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi |
(10 rows)

